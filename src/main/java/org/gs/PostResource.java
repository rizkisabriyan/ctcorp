package org.gs;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("post")
public class PostResource {

    @Inject
    PgPool client;

    @PostConstruct
    void config() {
        initdb();
    }

    @GET
    public Multi<Post> getAll() {
        return Post.findAll(client);
    }

    @GET
    @Path("{id}")
    public Uni<Response> get(@PathParam("id") Long id) {
        return Post.findById(client, id)
                .onItem()
                .transform(post -> post != null ? Response.ok(post) : Response.status(Response.Status.NOT_FOUND))
                .onItem()
                .transform(Response.ResponseBuilder::build);
    }

    @POST
    public Uni<Response> create(Post post) {
        return Post.save(client, post.getTitle(),post.getContent(),post.getTags())
                .onItem()
                .transform(id -> URI.create("/post/" + id))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam("id") Long id) {
        return Post.delete(client, id)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    private void initdb() {
        client.query("DROP TABLE IF EXISTS post").execute()
                .flatMap(m-> client.query("CREATE TABLE post (id SERIAL PRIMARY KEY, " +
                        "title TEXT NOT NULL," +
                        "content TEXT NOT NULL," +
                        "tags TEXT NOT NULL)").execute())
                .flatMap(m -> client.query("INSERT INTO post (title,content,tags) VALUES('Rizki','percobaan1','001')").execute())
                .flatMap(m -> client.query("INSERT INTO post (title,content,tags) VALUES('Sabriyan','percobaan2','002')").execute())
                .await()
                .indefinitely();
    }

}
