package org.gs;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import io.vertx.sqlclient.RowSet;

public class Post {

    private Long id;
    private String title;
    private String content;
    private String tags;

    public Post(Long id, String title, String content, String tags) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public static Multi<Post> findAll(PgPool client) {
        return client
                .query("SELECT id, title, content, tags FROM post ORDER BY title ASC")
                .execute()
                .onItem()
                .transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem()
                .transform(Post::from);
    }

    public static Uni<Post> findById(PgPool client, Long id) {
        return client
                .preparedQuery("SELECT id, title, content, tags FROM post WHERE id = $1")
                .execute(Tuple.of(id))
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<Long> save(PgPool client, String title,String content,String tags) {
        return client
                .preparedQuery("INSERT INTO post (title,content,tags) VALUES ($1,$2,$3) RETURNING id")
                .execute(Tuple.of(title,content,tags))
                .onItem()
                .transform(m -> m.iterator().next().getLong("id"));
    }

    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client
                .preparedQuery("DELETE FROM post WHERE id = $1")
                .execute(Tuple.of(id))
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static Post from(Row row ) {
        return new Post(row.getLong("id"), row.getString("title"), row.getString("content"), row.getString("tags"));
    }
}
