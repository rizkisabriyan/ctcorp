package org.gs;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("tag")
public class TagResource {

    @Inject
    PgPool client;

    @PostConstruct
    void config() {
        initdb();
    }

    @GET
    public Multi<Tag> getAll() {
        return Tag.findAll(client);
    }

    @GET
    @Path("{id}")
    public Uni<Response> get(@PathParam("id") Long id) {
        return Tag.findById(client, id)
                .onItem()
                .transform(tag -> tag != null ? Response.ok(tag) : Response.status(Response.Status.NOT_FOUND))
                .onItem()
                .transform(Response.ResponseBuilder::build);
    }

    @POST
    public Uni<Response> create(Tag tag) {
        return Tag.save(client, tag.getLabel(),tag.getPost())
                .onItem()
                .transform(id -> URI.create("/post/" + id))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam("id") Long id) {
        return Tag.delete(client, id)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    private void initdb() {
        client.query("DROP TABLE IF EXISTS tag").execute()
                .flatMap(m-> client.query("CREATE TABLE tag (id SERIAL PRIMARY KEY, " +
                        "label TEXT NOT NULL," +
                        "post TEXT NOT NULL)").execute())
                .flatMap(m -> client.query("INSERT INTO post (label,post) VALUES('Rizkipercobaan1','001')").execute())
                .flatMap(m -> client.query("INSERT INTO post (label,post) VALUES('Sabriyanpercobaan2','002')").execute())
                .await()
                .indefinitely();
    }

}
