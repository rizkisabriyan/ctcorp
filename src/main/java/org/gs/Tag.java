package org.gs;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import io.vertx.sqlclient.RowSet;

public class Tag {

    private Long id;
    private String label;
    private String post;

    public Tag(Long id, String label, String post) {
        this.id = id;
        this.label = label;
        this.post = post;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() { return label; }

    public void setLabel(String label) { this.label = label; }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public static Multi<Tag> findAll(PgPool client) {
        return client
                .query("SELECT id, label, post FROM tag ORDER BY label ASC")
                .execute()
                .onItem()
                .transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem()
                .transform(Tag::from);
    }

    public static Uni<Tag> findById(PgPool client, Long id) {
        return client
                .preparedQuery("SELECT id, label, post FROM tag WHERE id = $1")
                .execute(Tuple.of(id))
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<Long> save(PgPool client, String label,String post) {
        return client
                .preparedQuery("INSERT INTO tag (label,post) VALUES ($1,$2) RETURNING id")
                .execute(Tuple.of(label,post))
                .onItem()
                .transform(m -> m.iterator().next().getLong("id"));
    }

    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client
                .preparedQuery("DELETE FROM tag WHERE id = $1")
                .execute(Tuple.of(id))
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static Tag from(Row row ) {
        return new Tag(row.getLong("id"), row.getString("label"), row.getString("post"));
    }
}
